﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RouletteAPI.RequestModel
{
    public class BetRequest
    {
        public int RouletteId { get; set; }
        public string BetValue { get; set; }
        public decimal Amount { get; set; }
    }
}
