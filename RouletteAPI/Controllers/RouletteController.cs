﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using RouletteAPI.ActionFilters;
using RouletteAPI.Common;
using RouletteAPI.Interface;
using RouletteAPI.Model;
using RouletteAPI.RequestModel;

namespace RouletteAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RouletteController : ControllerBase
    {
        private readonly IRouletteService rouletteService;

        public RouletteController(IRouletteService rouletteService)
        {
            this.rouletteService = rouletteService;
        }
        [Route("GetAll")]
        [HttpGet]
        public IActionResult GetAll()
        {
           
            return new JsonResult(new {DB=rouletteService.GetAll() });
        }

        [Route("CreateRoulette")]
        [HttpPost]
        public IActionResult CreateRoulette()
        {
            int id = rouletteService.CreateRoulette();
            return new JsonResult(new { Id = id });
        }
        [Route("OpenRoulette")]
        [HttpPut]
        public IActionResult OpenRoulette([FromBody]RouletteIdentifier roulette)
        {
            bool result = rouletteService.OpenRoulette(idRoulette: roulette.idRoulette);
            return new JsonResult(new { Result = result ? "succed" : "denied" });
        }
        [UserAuthentication]
        [Route("MakeBet")]
        [HttpPost]
        public IActionResult MakeBet([FromBody]BetRequest bet)
        {
            try
            {
                bool result = false;
                if (Validate(bet))
                {
                    result = rouletteService.MakeBet(new Bet { UserId = GetUserId(Request), Amount = bet.Amount, BetValue = GetBetValue(bet, out bool isColor), RouletteId = bet.RouletteId, BetColor = isColor });
                }
                return new JsonResult(new { Result = result ? "succed" : "denied" });
            }
            catch (Exception e)
            {
                return new JsonResult(new { Result = $"error - {e.Message}" });
            }
        }

        [UserAuthentication]
        [Route("CloseRoulette")]
        [HttpPost]
        public IActionResult CloseRoulette([FromBody]RouletteIdentifier roulette)
        {
            try
            {
                return new JsonResult(new { Result = rouletteService.CloseRoulette(roulette.idRoulette) });
            }
            catch (Exception e)
            {
                return new JsonResult(new { Result = $"error - {e.Message}" });
            }
        }
        private int GetUserId(HttpRequest request)
        {
            return Int32.Parse(request.Headers["userId"]);
        }
        private bool Validate(BetRequest bet)
        {
            if (bet.Amount <= 0 || bet.Amount > 10000)
                throw new Exception("Invalid amount. Please enter a valid amount between 1$ and 10.000$");
            if (string.IsNullOrEmpty(bet.BetValue))
                throw new Exception("Invalid value. Please provide a valid value");
            int betNumberAmount = -1;
            if (!Int32.TryParse(bet.BetValue, out betNumberAmount) || betNumberAmount < 0 || betNumberAmount > 36)
            {
                if (bet.BetValue.ToLower() != "black" && bet.BetValue.ToLower() != "red")
                    throw new Exception("Invalid Bet value. please enter a valid Value (Red,Black or 0-36)");
            }
            return true;
        }
        private int GetBetValue(BetRequest bet, out bool isColor)
        {
            int BetValue = -1;
            if (Int32.TryParse(bet.BetValue, out BetValue))
            {
                isColor = false;
                return BetValue;
            }
            else
            {
                isColor = true;
                return bet.BetValue.ToLower() == "red" ? 0 : 1;
            }
        }

    }
}