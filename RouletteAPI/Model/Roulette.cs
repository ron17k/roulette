﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RouletteAPI.Model
{
    public class Roulette
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public int SelectedNumber { get; set; }
    }
}
