﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RouletteAPI.Model
{
    public class Bet
    {
        public int Id { get; set; }
        public int RouletteId{get;set;}
        public int BetValue { get; set; }
        public bool BetColor { get; set; }
        public decimal Amount { get; set; }
        public int UserId { get; set;}
    }
}
