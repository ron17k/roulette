﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RouletteAPI.Model
{
    public class BetResult : Bet
    {
        public string Result { get; set; }
        public string Color { get { if (BetColor) { return BetValue == 0 ? "Red" : "Black"; } else { return string.Empty; } } }
        public void SetResult(int Value)
        {
            if (this.BetColor)
            {
                if (Value % 2 == 0 && BetValue == 0)
                {
                    Result = $"WON {(Amount * 1.8m).ToString("c")} With Color RED ";
                }
                else if (Value % 2 != 0 && BetValue == 1)
                {
                    Result = $"WON {(Amount * 1.8m).ToString("c")} With Color BLACK ";
                }
                else
                {
                    Result = $"Lost {Amount.ToString("c")}";
                }
            }
            else {
                if (Value == BetValue) {
                    Result = $"WON {(Amount * 5m).ToString("c")} with Number {Value} ";
                }
                else
                {
                    Result = $"Lost {Amount.ToString("c")}";
                }
            }
        }
    }
}
