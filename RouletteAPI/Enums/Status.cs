﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RouletteAPI.Enums
{
    public enum Status
    {
        NEW,
        OPEN,
        CLOSED
    }
}
