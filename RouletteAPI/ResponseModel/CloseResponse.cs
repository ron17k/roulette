﻿
using RouletteAPI.Model;
using System.Collections.Generic;

namespace RouletteAPI.ResponseModel
{
    public class CloseResponse
    {
        public string RouletteName { get; set; }
        public int SelectedNumber { get; set; }
        public string Color { get { if (SelectedNumber % 2 == 0) { return "Red"; } else { return "Black"; } } }
        public IEnumerable<BetResult> Result { get; set; }
    }
}
