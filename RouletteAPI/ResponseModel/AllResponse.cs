﻿using RouletteAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RouletteAPI.ResponseModel
{
    public class AllResponse
    {
        public IEnumerable<Roulette> Roulettes { get; set; }
        public IEnumerable<Bet> Bets { get; set; }
    }
}
