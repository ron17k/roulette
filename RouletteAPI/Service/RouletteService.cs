﻿using Microsoft.Extensions.Caching.Distributed;
using RouletteAPI.Common;
using RouletteAPI.Enums;
using RouletteAPI.Interface;
using RouletteAPI.Model;
using RouletteAPI.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace RouletteAPI.Service
{
    public class RouletteService : IRouletteService
    {
        private readonly IDistributedCache distributedCache;
        public RouletteService(IDistributedCache distributedCache)
        {
            this.distributedCache = distributedCache;
        }
        public int CreateRoulette()
        {
            Roulette roulette = new Roulette { Id = GetNextId(), Status = Status.NEW.ToString() };
            Add<Roulette>(roulette);
            return roulette.Id;
        }
        public IEnumerable<T> GetList<T>(string key)
        {
            var list = distributedCache.GetStringAsync(key);
            if (list == null)
            {
                distributedCache.SetStringAsync(key, JsonSerializer.Serialize(new List<T>()));
                return new List<T>();
            }
            else
            {
                return ConverJsonSrtringtToList<T>(list.Result);
            }
        }
        public bool MakeBet(Bet bet)
        {
            if (Validate(bet))
            {
                bet.Id = GetNextId();
                Add<Bet>(bet);
            }
            return true;
        }
        public bool OpenRoulette(int idRoulette)
        {
            return ChangeStatusRoulette(idRoulette, Status.OPEN);
        }
        public CloseResponse CloseRoulette(int idRoulette)
        {
            Random random = new Random();
            int selectedNumber = random.Next(0, 36);
            List<BetResult> result = GetBetsByRouletteId(idRoulette).ToList();
            result.ForEach(m => { m.SetResult(selectedNumber); });
            UpdateRoulette(idRoulette, Status.CLOSED,selectedNumber);
            return new CloseResponse
            {
                RouletteName = $"Roulette {idRoulette}",
                SelectedNumber = selectedNumber,
                Result = result
            };
        }
        public AllResponse GetAll() {
            return new AllResponse {
               Roulettes= GetList<Roulette>(KeyDictionary.RouletteList),
               Bets = GetList<Bet>(KeyDictionary.BetList),
            };
        }
        private bool ChangeStatusRoulette(int idRoulette, Status newStatus)
        {
            IEnumerable<Roulette> rouletteList = GetList<Roulette>(KeyDictionary.RouletteList);
            Roulette selectedRoulette = rouletteList.Where(m => m.Id == idRoulette).SingleOrDefault();
            if (selectedRoulette != null)
            {
                rouletteList.Where(m => m.Id == idRoulette).SingleOrDefault().Status = newStatus.ToString();
                distributedCache.SetString(KeyDictionary.RouletteList, JsonSerializer.Serialize(rouletteList));
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool UpdateRoulette(int idRoulette, Status newStatus,int selectedNumber)
        {
            IEnumerable<Roulette> rouletteList = GetList<Roulette>(KeyDictionary.RouletteList);
            Roulette selectedRoulette = rouletteList.Where(m => m.Id == idRoulette).SingleOrDefault();
            if (selectedRoulette != null)
            {
                rouletteList.Where(m => m.Id == idRoulette).SingleOrDefault().Status = newStatus.ToString();
                rouletteList.Where(m => m.Id == idRoulette).SingleOrDefault().SelectedNumber = selectedNumber;
                distributedCache.SetString(KeyDictionary.RouletteList, JsonSerializer.Serialize(rouletteList));
                return true;
            }
            else
            {
                return false;
            }
        }
        private int GetNextId()
        {
            var value = distributedCache.Get(KeyDictionary.CounterId);
            if (value == null)
            {
                distributedCache.Set(KeyDictionary.CounterId, BitConverter.GetBytes(1));
                return 1;
            }
            else
            {
                int currentValue = BitConverter.ToInt32(value);
                distributedCache.Set(KeyDictionary.CounterId, BitConverter.GetBytes(++currentValue));
                return currentValue;
            }
        }
        private void Add<T>(T entity)
        {
            string key = GetKey(entity);
            IEnumerable<T> rouletteLsit = GetList<T>(key).Append(entity);
            distributedCache.SetString(key, JsonSerializer.Serialize(rouletteLsit));
        }
        private string GetKey<T>(T entity)
        {
            if (entity is Roulette)
                return KeyDictionary.RouletteList;
            else if (entity is Bet)
                return KeyDictionary.BetList;
            return string.Empty;
        }
        private IEnumerable<T> ConverJsonSrtringtToList<T>(string listJson)
        {
            if (string.IsNullOrEmpty(listJson))
            {
                return new List<T>();
            }
            else
            {
                return JsonSerializer.Deserialize<List<T>>(listJson);
            }
        }
        private IEnumerable<BetResult> GetBetsByRouletteId(int rouletteId)
        {
            return GetList<BetResult>(KeyDictionary.BetList).Where(m => m.RouletteId == rouletteId);
        }
        private bool Validate(Bet bet)
        {
            Roulette roulette = GetList<Roulette>(KeyDictionary.RouletteList).Where(m => m.Id == bet.RouletteId && m.Status == Status.OPEN.ToString()).SingleOrDefault();
            if (roulette == null)
            {
                throw new Exception("The Roulette is alreaady closed or does not exist.");
            }
            IEnumerable<Bet> betList = GetList<Bet>(KeyDictionary.BetList);
            if (betList.Any(m => m.RouletteId == bet.RouletteId && m.UserId == bet.UserId && m.BetColor == bet.BetColor && m.BetValue == bet.BetValue))
            {
                throw new Exception("The user already has this bet on the table");
            }
            return true;
        }
    }
}
