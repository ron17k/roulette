﻿using RouletteAPI.Model;
using RouletteAPI.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RouletteAPI.Interface
{
    public interface IRouletteService
    {
        int CreateRoulette();
        bool MakeBet(Bet bet);
        public IEnumerable<T> GetList<T>(string key);
        bool OpenRoulette(int idRoulette);
        CloseResponse CloseRoulette(int idRoulette);
        AllResponse GetAll();
    }
}
