﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace RouletteAPI.ActionFilters
{
    public class UserAuthentication : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            int result = 0;
            if (string.IsNullOrEmpty(context.HttpContext.Request.Headers["UserId"]) || !Int32.TryParse(context.HttpContext.Request.Headers["UserId"],out result)) {
                context.Result = new UnauthorizedObjectResult("user is unauthorized");
                return;
            }
            base.OnActionExecuting(context);
        }
    }
}
